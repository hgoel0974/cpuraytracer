﻿using CPURaytracing.VMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPURaytracing.RayTracing.Materials
{
    public class DiffuseMaterial : IMaterial
    {
        public Vector4 Color;
        NRandom rng = new NRandom(0);

        public void Compute(Scene parent, Vector3 point, Ray incomingRay, Vector3 normal, Vector2 uv, int max_bounces, out Vector4 color)
        {
            //The next bounce is at an equal angle to the normal
            Vector3 reflected_dir = incomingRay.Direction - 2 * Vector3.Dot(normal, incomingRay.Direction) * normal;
            Ray reflected = new Ray(point, reflected_dir);

            var diff_color = Vector4.Zero;
            if (max_bounces > 0)
            {
                //Determine if this surface is visible at all from the lights
                color = Vector4.Zero;
                for(int i = 0; i < parent.Lights.Count; i++)
                {
                    color += parent.Lights[i].Radiance(parent, point, normal);
                }
                color = Vector4.Multiply(color, Color);

                //Compute the incoming light
                /*var diff_coeff = Math.Max(0, Vector3.Dot(normal, reflected_dir));
                if (parent.Trace(reflected, out var closestDist, out var closestNorm, out var closestUv, out var closestPrim))
                {
                    closestPrim.Material.Compute(parent, point + reflected_dir * (closestDist - parent.Epsilon), reflected, closestNorm, closestUv, max_bounces - 1, out var p_color);
                    color = diff_coeff * Vector4.Multiply(diff_color, Color);
                }
                else
                {
                    color = diff_coeff * Vector4.Multiply(diff_color, Color);
                }*/
                //color = diff_color;
            }
            else
            {
                color = Vector4.Zero;
                return;
            }
        }
    }
}
