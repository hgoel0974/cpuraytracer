﻿using CPURaytracing.RayTracing.Materials;
using CPURaytracing.VMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vec3 = System.Numerics.Vector3;
using vec = System.Numerics.Vector<float>;

namespace CPURaytracing.RayTracing.Primitives
{
    public class Sphere : IPrimitive
    {
        public Vector3 Center;
        public float Radius;

        public Sphere(Vector3 center, float rad, int id)
        {
            Center = center;
            Radius = rad;
            ID = id;
        }

        public IMaterial Material { get; set; }
        public int ID { get; set; }

        public bool Intersects(Scene parent, Ray r, out float dist, out Vector3 normal, out Vector2 uv)
        {
            vec3 orig = new vec3(r.Origin.X, r.Origin.Y, r.Origin.Z);
            vec3 dir = new vec3(r.Direction.X, r.Direction.Y, r.Direction.Z);
            vec3 center = new vec3(Center.X, Center.Y, Center.Z);

            var oc = orig - center;
            float b = vec3.Dot(oc, dir);
            float c = vec3.Dot(oc, oc) - Radius * Radius;
            if (c > 0.0f && b > 0.0f)
            {
                dist = -1;
                normal = Vector3.Zero;
                uv = Vector2.Zero;
                return false;
            }

            float discriminant = (b * b - c);
            if (discriminant < 0)
            {
                dist = -1;
                normal = Vector3.Zero;
                uv = Vector2.Zero;
                return false;
            }
            else
            {
                dist = (float)(-b - Math.Sqrt(discriminant));
                if (dist < 0)
                {
                    dist = -1;
                    normal = Vector3.Zero;
                    uv = Vector2.Zero;
                    return false;
                }

                var n = vec3.Normalize((orig + dir * dist) - center);
                normal = new Vector3(n.X, n.Y, n.Z);
                uv = normal.Xy;
                return true;
            }
        }

        public bool Intersects(Scene parent, Ray[] r, out float[] dist, out Vector3[] normal, out Vector2[] uv)
        {
            int batch_sz = vec.Count;
            int iter_cnt = r.Length / batch_sz;
            if (r.Length % batch_sz != 0) iter_cnt++;
            int r_idx = 0;

            for(int idx = 0; idx < iter_cnt; idx++)
            {
                float[] origX_f = new float[batch_sz];
                float[] origY_f = new float[batch_sz];
                float[] origZ_f = new float[batch_sz];

                float[] dirX_f = new float[batch_sz];
                float[] dirY_f = new float[batch_sz];
                float[] dirZ_f = new float[batch_sz];

                float[] centerX_f = new float[batch_sz];
                float[] centerY_f = new float[batch_sz];
                float[] centerZ_f = new float[batch_sz];
                for (int i = 0; i < batch_sz; i++)
                {
                    if (r_idx < r.Length)
                    {
                        origX_f[i] = r[r_idx].Origin.X;
                        origY_f[i] = r[r_idx].Origin.Y;
                        origZ_f[i] = r[r_idx].Origin.Z;

                        dirX_f[i] = r[r_idx].Direction.X;
                        dirY_f[i] = r[r_idx].Direction.Y;
                        dirZ_f[i] = r[r_idx].Direction.Z;

                        centerX_f[i] = Center.X;
                        centerY_f[i] = Center.Y;
                        centerZ_f[i] = Center.Z;
                    }
                    r_idx++;
                }

                Vector3Batch orig = new Vector3Batch(origX_f, origY_f, origZ_f);
                Vector3Batch dir = new Vector3Batch(dirX_f, dirY_f, dirZ_f);
                Vector3Batch center = new Vector3Batch(centerX_f, centerY_f, centerZ_f);

                var oc = orig - center;
                var b = Vector3Batch.Dot(oc, dir);
                var c = Vector3Batch.Dot(oc, oc) - new vec(Radius * Radius);
                var distances = (-b - Math.Sqrt(discriminant));
                if (c > 0.0f && b > 0.0f)
                {
                    dist = -1;
                    normal = Vector3.Zero;
                    uv = Vector2.Zero;
                    return false;
                }

                float discriminant = (b * b - c);
                if (discriminant < 0)
                {
                    dist = -1;
                    normal = Vector3.Zero;
                    uv = Vector2.Zero;
                    return false;
                }
                else
                {
                    dist = (float)(-b - Math.Sqrt(discriminant));
                    if (dist < 0)
                    {
                        dist = -1;
                        normal = Vector3.Zero;
                        uv = Vector2.Zero;
                        return false;
                    }

                    var n = vec3.Normalize((orig + dir * dist) - center);
                    normal = new Vector3(n.X, n.Y, n.Z);
                    uv = normal.Xy;
                    return true;
                }
            }
        }
    }
}
