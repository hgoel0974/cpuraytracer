﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPURaytracing.RayTracing.Materials;
using CPURaytracing.VMath;

namespace CPURaytracing.RayTracing.Primitives
{
    public class Triangle : IPrimitive
    {
        Vector3[] Corners;
        Vector3 Normal;

        public int ID { get; set; }
        public IMaterial Material { get; set; }

        public Triangle(Vector3 v0, Vector3 v1, Vector3 v2, Vector3 n)
        {
            Corners = new Vector3[] { v0, v1, v2 };
            Normal = n;
        }

        public bool Intersects(Scene parent, Ray r, out float dist, out Vector3 normal, out Vector2 uv)
        {
            int cnt = System.Numerics.Vector<int>.Count;

            Vector3 vertex0 = Corners[0];
            Vector3 vertex1 = Corners[1];
            Vector3 vertex2 = Corners[2];
            Vector3 edge1 = vertex1 - vertex0;
            Vector3 edge2 = vertex2 - vertex0;
            Vector3 h = Vector3.Cross(r.Direction, edge2);
            Vector3 s = new Vector3();
            Vector3 q = new Vector3();
            float a, f, u, v;
            a = Vector3.Dot(edge1, h); ;
            if (a > -parent.Epsilon && a < parent.Epsilon)
            {
                dist = -1;
                normal = Vector3.Zero;
                uv = Vector2.Zero;
                return false;    // This ray is parallel to this triangle.
            }
            f = 1.0f / a;
            s = r.Origin - vertex0;
            u = f * Vector3.Dot(s, h);
            if (u < 0.0 || u > 1.0)
            {
                dist = -1;
                normal = Vector3.Zero;
                uv = Vector2.Zero;
                return false;
            }
            q = Vector3.Cross(s, edge1);
            v = f * Vector3.Dot(r.Direction, q);
            if (v < 0.0 || u + v > 1.0)
            {
                dist = -1;
                normal = Vector3.Zero;
                uv = Vector2.Zero;
                return false;
            }
            // At this stage we can compute t to find out where the intersection point is on the line.
            float t = f * Vector3.Dot(edge2, q);
            if (t > parent.Epsilon) // ray intersection
            {
                dist = t;
                normal = Normal;
                uv = Vector2.Zero;
                return true;
            }
            else // This means that there is a line intersection but not a ray intersection.
            {
                dist = -1;
                normal = Vector3.Zero;
                uv = Vector2.Zero;
                return false;
            }
        }

        public bool Intersects(Scene parent, Ray[] r, out float[] dist, out Vector3[] normal, out Vector2[] uv)
        {
            throw new NotImplementedException();
        }
    }
}
