﻿using CPURaytracing.VMath;
using CPURaytracing.RayTracing.Materials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPURaytracing.RayTracing.Primitives
{
    public interface IPrimitive
    {
        int ID { get; set; }
        IMaterial Material { get; set; }
        bool Intersects(Scene parent, Ray r, out float dist, out Vector3 normal, out Vector2 uv);
        bool Intersects(Scene parent, Ray[] r, out float[] dist, out Vector3[] normal, out Vector2[] uv);
    }
}
