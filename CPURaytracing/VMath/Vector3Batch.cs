﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vec = System.Numerics.Vector<float>;

namespace CPURaytracing.VMath
{
    public class Vector3Batch
    {
        public vec X;
        public vec Y;
        public vec Z;

        public Vector3Batch(float[] x, float[] y, float[] z)
        {
            X = new vec(x);
            Y = new vec(y);
            Z = new vec(z);
        }

        public Vector3Batch(vec x, vec y, vec z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static vec Dot(Vector3Batch a, Vector3Batch b)
        {
            return a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        }

        public static Vector3Batch operator -(Vector3Batch a, Vector3Batch b)
        {
            return new Vector3Batch(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        }
    }
}
