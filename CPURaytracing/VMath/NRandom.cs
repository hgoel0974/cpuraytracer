﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CPURaytracing.VMath
{
    public class NRandom
    {
        private ulong seed;

        public NRandom(ulong seed)
        {
            this.seed = seed;
        }

        public int Next()
        {
            var val = unchecked(6364136223846793005 * seed + 1);
            seed = val;
            return (int)(val >> 33);
        }

        public float NextFloat()
        {
            return (float)((double)Next() / int.MaxValue);
        }
    }
}
