﻿using CPURaytracing.RayTracing;
using CPURaytracing.RayTracing.Lights;
using CPURaytracing.RayTracing.Materials;
using CPURaytracing.RayTracing.Primitives;
using CPURaytracing.VMath;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CPURaytracing
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Size = new Size(1920/2, 1080/2);
            DoubleBuffered = true;
            prevpos = pos = Vector3.UnitZ * 2;
            target_spherical = Vector3.ToSpherical(Vector3.UnitY);
        }

        int click_cnt = 2;
        Vector3 pos, prevpos;
        Vector3 target_spherical;
        Scene scene;

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            int w = this.Width;
            int h = this.Height;

            if (scene == null || (w != scene.Width | h != scene.Height))
            {
                scene = new Scene(w, h, 90, pos, Vector3.FromSpherical(target_spherical), Vector3.UnitZ, 1e10f);
                scene.UpdateViews();
                prevpos = pos;
                scene.ClearColor = new Vector4(0, 0.5f, 1.0f, 1.0f);
                scene.AddPrimitive(new Sphere(Vector3.UnitY * click_cnt, 1, 0) { Material = new DiffuseMaterial() { Color = Vector4.One } });
                scene.AddPrimitive(new Sphere(Vector3.UnitY * click_cnt + Vector3.UnitZ * 8, 4, 1) { Material = new DiffuseMaterial() { Color = Vector4.UnitX } });
                scene.AddPrimitive(new Triangle(Vector3.UnitY * 6 + Vector3.UnitZ * 8, Vector3.UnitY * 6 + Vector3.UnitZ * -8, Vector3.UnitY * 6 + Vector3.UnitX * 8, -Vector3.UnitY) { Material = new DiffuseMaterial() { Color = Vector4.UnitX } });
                //scene.AddLight(new PointLight(Vector3.UnitY * click_cnt + Vector3.UnitZ * 2, Vector3.One, 1));
                scene.AddLight(new PointLight(Vector3.UnitY * click_cnt + Vector3.UnitZ * -5, Vector3.One, 100));
                //Spherical lights by tracing cones in the direction of the light source
                //
                //scene.AddPrimitive(new Sphere(Vector3.UnitY * click_cnt, 1, 0) { Material = new NormalMaterial() });
            }

            Stopwatch updateTimer = new Stopwatch();
            updateTimer.Start();
            if (prevpos != pos)
            {
                scene.SetCameraParams(pos, Vector3.FromSpherical(target_spherical), Vector3.UnitZ);
                scene.UpdateViews();
                prevpos = pos;
            }
            updateTimer.Stop();

            Bitmap bmp = new Bitmap(w, h);
            Stopwatch timer = new Stopwatch();
            timer.Start();
            scene.Render(bmp, 2);
            timer.Stop();
            this.Text = $"({Width},{Height}), {scene.rayCnt}, Render: {TimeSpan.FromMilliseconds(timer.ElapsedMilliseconds).ToString()}, Update: {TimeSpan.FromMilliseconds(updateTimer.ElapsedMilliseconds).ToString()}";

            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            e.Graphics.DrawImage(bmp, 0, 0, Width, Height);
            bmp.Dispose();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Refresh();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'w')
            {
                pos += Vector3.UnitY * 0.05f;
            }
            if (e.KeyChar == 's')
            {
                pos -= Vector3.UnitY * 0.05f;
            }
            if (e.KeyChar == 'a')
            {
                pos -= Vector3.UnitX * 0.05f;
            }
            if (e.KeyChar == 'd')
            {
                pos += Vector3.UnitX * 0.05f;
            }
            if (e.KeyChar == 'q')
            {
                pos -= Vector3.UnitZ * 0.05f;
            }
            if (e.KeyChar == 'e')
            {
                pos += Vector3.UnitZ * 0.05f;
            }
            if (e.KeyChar == 'i')
            {
                target_spherical -= Vector3.UnitY * 0.02f;
            }
            if (e.KeyChar == 'k')
            {
                target_spherical += Vector3.UnitY * 0.02f;
            }
            if (e.KeyChar == 'j')
            {
                target_spherical -= Vector3.UnitZ * 0.02f;
            }
            if (e.KeyChar == 'l')
            {
                target_spherical += Vector3.UnitZ * 0.02f;
            }
            if (e.KeyChar == '+')
            {
                click_cnt++;
            }
            if (e.KeyChar == '-')
            {
                click_cnt--;
            }
            Refresh();
        }
    }
}
