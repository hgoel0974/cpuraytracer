﻿using CPURaytracing.VMath;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace CPURaytracing.RayTracing
{
    public class Camera
    {
        public Matrix4 View;
        public Matrix4 Projection;

        public Vector3 Position;
        public Vector3 Direction;
        public Vector3 Up;

        public int Width;
        public int Height;

        private Vector3[] rayDirs;
        private float fov;

        public Camera(int w, int h, float fov, Vector3 pos, Vector3 dir, Vector3 up, float dist_max)
        {
            View = Matrix4.LookAt(pos, pos + dir, up);
            Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(fov), w / (float)h, 0.001f);

            Width = w;
            Height = h;
            Position = pos;
            Direction = dir;
            Up = up;
            this.fov = fov;

            rayDirs = new Vector3[w * h];
        }

        public void SetParams(Vector3 pos, Vector3 dir, Vector3 up)
        {
            View = Matrix4.LookAt(pos, pos + dir, up);
            Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(fov), Width / (float)Height, 0.001f);
            Position = pos;
            Direction = dir;
            Up = up;
        }

        int h_div = 20;
        int w_div = 64;
        public void Update()
        {
            float x_stride = 2.0f / Width;
            float y_stride = 2.0f / Height;

            var VP = View * Projection;
            var iVP = Matrix4.Invert(VP);

            int block_len = h_div * w_div;

            int h_blocks = Height / h_div;
            int w_blocks = Width / w_div;
            int block_cnt = h_blocks * w_blocks;

            //Compute start pixel position
            Parallel.For(0, block_cnt, (block_idx) =>
            {
                int px_base = block_idx % w_blocks * w_div;
                int py_base = block_idx / w_blocks * h_div;

                for (int i = 0; i < block_len; i++)
                {
                    int px = px_base + i % w_div;
                    int py = py_base + i / w_div;

                    var x = (px - Width / 2.0f) / Width;
                    var y = (-py + Height / 2.0f) / Height;

                    Vector4 ray_e_clip = new Vector4(x, y, 1, 1);
                    Vector4 ray_e_world_ndc = Vector4.Transform(ray_e_clip, iVP);

                    Vector3 ray_o_world = Position;
                    Vector3 ray_e_world = ray_e_world_ndc.Xyz / ray_e_world_ndc.W;

                    Vector3 ray_dir_world = (ray_e_world - ray_o_world);
                    ray_dir_world = Vector3.Normalize(ray_dir_world);

                    rayDirs[block_idx * block_len + i] = ray_dir_world;
                }
            });
        }

        public void Render(Scene parent, Bitmap dst, int bounces)
        {
            float x_stride = 2.0f / Width;
            float y_stride = 2.0f / Height;

            var VP = View * Projection;
            var iVP = Matrix4.Invert(VP);

            var gdata = dst.LockBits(new Rectangle(0, 0, Width, Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            //Batch 8x4 blocks

            int block_len = h_div * w_div;

            int h_blocks = Height / h_div;
            int w_blocks = Width / w_div;
            int block_cnt = h_blocks * w_blocks;

            //Compute start pixel position
            //Parallel.For(0, block_cnt, (block_idx) =>
            for(int block_idx = 0; block_idx < block_cnt; block_idx++)
            {
                int px_base = block_idx % w_blocks * w_div;
                int py_base = block_idx / w_blocks * h_div;

                Ray[] block_rays = new Ray[block_len];
                for (int i = 0; i < block_len; i++)
                {
                    int px = px_base + i % w_div;
                    int py = py_base + i / w_div;

                    block_rays[i] = new Ray(Position, rayDirs[block_idx * block_len + i]);
                }

                var intersected = parent.TraceBatch(block_rays, out var closestDist, out var closestNorm, out var closestUv, out var closestPrim);
                for (int i = 0; i < intersected.Length; i++)
                {
                    int px = px_base + i % w_div;
                    int py = py_base + i / w_div;

                    if (intersected[i])
                    {
                        closestPrim[i].Material.Compute(parent, block_rays[i].Origin + block_rays[i].Direction * (closestDist[i] - parent.Epsilon), block_rays[i], closestNorm[i], closestUv[i], bounces - 1, out var color);
                        if (color.X > 1) color.X = 1;
                        if (color.Y > 1) color.Y = 1;
                        if (color.Z > 1) color.Z = 1;
                        if (color.W > 1) color.W = 1;

                        if (color.X < 0) color.X = 0;
                        if (color.Y < 0) color.Y = 0;
                        if (color.Z < 0) color.Z = 0;
                        if (color.W < 0) color.W = 0;

                        unsafe
                        {
                            var len = gdata.Height * gdata.Stride;
                            var dst_ptr = (byte*)gdata.Scan0;
                            int idx = py * gdata.Stride + (px * 4);

                            dst_ptr[idx + 2] = (byte)(color.X * 255.0f);
                            dst_ptr[idx + 1] = (byte)(color.Y * 255.0f);
                            dst_ptr[idx + 0] = (byte)(color.Z * 255.0f);
                            dst_ptr[idx + 3] = (byte)(color.W * 255.0f);
                        }
                    }
                    else
                    {
                        unsafe
                        {
                            var len = gdata.Height * gdata.Stride;
                            var dst_ptr = (byte*)gdata.Scan0;
                            int idx = py * gdata.Stride + (px * 4);

                            dst_ptr[idx + 2] = (byte)(block_idx % w_blocks);//parent.ClearColor.X * 255.0f);
                            dst_ptr[idx + 1] = (byte)(block_idx / w_blocks);//parent.ClearColor.Y * 255.0f);
                            dst_ptr[idx + 0] = (byte)(parent.ClearColor.Z * 255.0f);
                            dst_ptr[idx + 3] = (byte)(parent.ClearColor.W * 255.0f);
                        }
                    }
                }
            }//);
            dst.UnlockBits(gdata);
        }
    }
}