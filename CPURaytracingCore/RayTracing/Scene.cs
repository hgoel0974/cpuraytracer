﻿using CPURaytracing.VMath;
using CPURaytracing.RayTracing.Primitives;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPURaytracing.RayTracing.Lights;

namespace CPURaytracing.RayTracing
{
    public class Scene
    {
        private Camera Camera;
        private List<IPrimitive> Primitives;
        internal List<ILight> Lights;

        public Vector4 ClearColor { get; set; }
        public float Epsilon = 1e-3f;

        public int Width { get; private set; }
        public int Height { get; private set; }

        public Scene(int w, int h, float fov, Vector3 pos, Vector3 dir, Vector3 up, float dist_max)
        {
            Primitives = new List<IPrimitive>();
            Lights = new List<ILight>();

            Camera = new Camera(w, h, fov, pos, dir, up, dist_max);

            Width = w;
            Height = h;
        }

        public void AddPrimitive(IPrimitive prim)
        {
            Primitives.Add(prim);
        }

        public void AddLight(ILight prim)
        {
            Lights.Add(prim);
        }

        public void Render(Bitmap bmp, int bounces)
        {
            Camera.Render(this, bmp, bounces);
        }

        public void UpdateViews()
        {
            Camera.Update();
        }

        public void SetCameraParams(Vector3 pos, Vector3 dir, Vector3 up)
        {
            Camera.SetParams(pos, dir, up);
        }

        public int rayCnt = 0;

        public bool[] TraceBatch(Ray[] r, out float[] dist, out Vector3[] normal, out Vector2[] uv, out IPrimitive[] prim)
        {
            var closestDist = new float[r.Length];// float.MaxValue;
            var closestNorm = new Vector3[r.Length];// Vector3.One;
            var closestUV = new Vector2[r.Length];//Vector2.One;
            var closestIdx = new int[r.Length];//-1;
            var ret_bool = new bool[r.Length];

            Array.Fill(closestDist, float.MaxValue);
            Array.Fill(closestIdx, -1);

            dist = new float[r.Length];
            normal = new Vector3[r.Length];
            uv = new Vector2[r.Length];
            prim = new IPrimitive[r.Length];

            Array.Fill(dist, -1);

            for (int i = 0; i < Primitives.Count; i++)
            {
                var intersects = Primitives[i].Intersects(this, r, out var curDist, out var curNorm, out var curUV);
                for (int j = 0; j < intersects.Length; j++)
                    if (intersects[j] && curDist[j] < closestDist[j] && curDist[j] > 0)
                    {
                        closestIdx[j] = i;
                        closestNorm[j] = curNorm[j];
                        closestUV[j] = curUV[j];
                        closestDist[j] = curDist[j];

                        dist[j] = curDist[j];
                        normal[j] = curNorm[j];
                        uv[j] = curUV[j];
                        prim[j] = Primitives[i];
                        ret_bool[j] = true;
                    }
            }
            return ret_bool;
        }
        public bool Trace(Ray r, out float dist, out Vector3 normal, out Vector2 uv, out IPrimitive prim)
        {
            //System.Threading.Interlocked.Increment(ref rayCnt);
            var closestDist = float.MaxValue;
            var closestNorm = Vector3.One;
            var closestUV = Vector2.One;
            var closestIdx = -1;
            for (int i = 0; i < Primitives.Count; i++)
            {
                var intersects = Primitives[i].Intersects(this, r, out float curDist, out var curNorm, out var curUV);
                if (intersects && curDist < closestDist && curDist > 0)
                {
                    closestIdx = i;
                    closestNorm = curNorm;
                    closestUV = curUV;
                    closestDist = curDist;
                }
            }
            if (closestIdx == -1)
            {
                dist = -1;
                normal = Vector3.One;
                prim = null;
                uv = Vector2.One;
                return false;
            }
            else
            {
                //Console.WriteLine($"{closestIdx},{closestDist}");
                dist = closestDist;
                normal = closestNorm;
                uv = closestUV;
                prim = Primitives[closestIdx];
                return true;
            }
        }
    }
}
