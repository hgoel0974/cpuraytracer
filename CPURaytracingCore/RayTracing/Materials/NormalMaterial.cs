﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPURaytracing.VMath;
using vec3 = System.Numerics.Vector3;

namespace CPURaytracing.RayTracing.Materials
{
    public class NormalMaterial : IMaterial
    {
        public void Compute(Scene parent, Vector3 point, Ray incomingDir, Vector3 normal, Vector2 uv, int max_bounces, out Vector4 color)
        {
            color = new Vector4(normal.X * 0.5f + 0.5f, normal.Y * 0.5f + 0.5f, normal.Z * 0.5f + 0.5f, 1);
            return;
        }
    }
}
