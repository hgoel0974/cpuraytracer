﻿using CPURaytracing.VMath;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPURaytracing.RayTracing.Lights
{
    public interface ILight
    {
        Vector4 Radiance(Scene parent, Vector3 incomingPoint, Vector3 incomingNorm);
    }
}
