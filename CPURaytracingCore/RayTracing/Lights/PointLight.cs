﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CPURaytracing.VMath;

namespace CPURaytracing.RayTracing.Lights
{
    public class PointLight : ILight
    {
        private Vector3 Color;
        private float Power;
        private Vector3 Position;

        public PointLight(Vector3 pos, Vector3 col, float pow)
        {
            Position = pos;
            Color = col;
            Power = pow;
        }

        public Vector4 Radiance(Scene parent, Vector3 incomingPoint, Vector3 incomingNorm)
        {
            var dir = Vector3.Normalize(Position - incomingPoint);
            var lDist = (Position - incomingPoint).Length;

            Ray r = new Ray(incomingPoint, dir);
            var intersection = parent.Trace(r, out var dist, out var n, out var uv, out var prim);
            if (intersection && dist < lDist)
            {
                //In shadow
                return new Vector4(0);
            }
            else
            {
                //In light
                return new Vector4(Color * Vector3.Dot(incomingNorm, dir), Power / (dist * dist));
            }
        }
    }
}
