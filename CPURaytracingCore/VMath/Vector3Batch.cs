﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vec = System.Runtime.Intrinsics.Vector256<float>;
using simd = System.Runtime.Intrinsics.X86.Avx2;

namespace CPURaytracing.VMath
{
    public class Vector3Batch
    {
        public FloatBatch X;
        public FloatBatch Y;
        public FloatBatch Z;

        public Vector3Batch(float[] x, float[] y, float[] z)
        {
            X = new FloatBatch(x);
            Y = new FloatBatch(y);
            Z = new FloatBatch(z);
        }
        public Vector3Batch()
        {
            X = new FloatBatch();
            Y = new FloatBatch();
            Z = new FloatBatch();
        }
        public Vector3Batch(FloatBatch x, FloatBatch y, FloatBatch z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector3[] Read()
        {
            Vector3[] res = new Vector3[FloatBatch.Count];

            float[] x = new float[vec.Count];
            float[] y = new float[vec.Count];
            float[] z = new float[vec.Count];

            X.Read(x, 0);
            Y.Read(y, 0);
            Z.Read(z, 0);

            for (int i = 0; i < res.Length; i++)
                res[i] = new Vector3(x[i], y[i], z[i]);
            return res;
        }

        public static void Dot(Vector3Batch a, Vector3Batch b, ref FloatBatch c)
        {
            c.Value = simd.Add(simd.Add(simd.Multiply(a.X.Value, b.X.Value), simd.Multiply(a.Y.Value, b.Y.Value)), simd.Multiply(a.Z.Value, b.Z.Value));
        }
        public static FloatBatch Dot(Vector3Batch a, Vector3Batch b)
        {
            return new FloatBatch(simd.Add(simd.Add(simd.Multiply(a.X.Value, b.X.Value), simd.Multiply(a.Y.Value, b.Y.Value)), simd.Multiply(a.Z.Value, b.Z.Value)));
        }
        public static Vector3Batch Normalize(Vector3Batch a)
        {
            var div = new FloatBatch(simd.ReciprocalSqrt(Dot(a, a).Value));
            return a * div;
        }

        public static void Subtract(Vector3Batch a, Vector3Batch b, ref Vector3Batch res)
        {
            res.X.Value = simd.Subtract(a.X.Value, b.X.Value);
            res.Y.Value = simd.Subtract(a.Y.Value, b.Y.Value);
            res.Z.Value = simd.Subtract(a.Z.Value, b.Z.Value);
        }
        public static Vector3Batch operator -(Vector3Batch a, Vector3Batch b)
        {
            return new Vector3Batch(new FloatBatch(simd.Subtract(a.X.Value, b.X.Value)), new FloatBatch(simd.Subtract(a.Y.Value, b.Y.Value)), new FloatBatch(simd.Subtract(a.Z.Value, b.Z.Value)));
        }
        public static Vector3Batch operator +(Vector3Batch a, Vector3Batch b)
        {
            return new Vector3Batch(new FloatBatch(simd.Add(a.X.Value, b.X.Value)), new FloatBatch(simd.Add(a.Y.Value, b.Y.Value)), new FloatBatch(simd.Add(a.Z.Value, b.Z.Value)));
        }
        public static Vector3Batch operator *(Vector3Batch a, FloatBatch b)
        {
            return new Vector3Batch(new FloatBatch(simd.Multiply(a.X.Value, b.Value)), new FloatBatch(simd.Multiply(a.Y.Value, b.Value)), new FloatBatch(simd.Multiply(a.Z.Value, b.Value)));
        }
    }
}
