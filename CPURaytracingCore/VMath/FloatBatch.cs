﻿using System;
using System.Collections.Generic;
using System.Text;
using vec = System.Runtime.Intrinsics.Vector256<float>;
using simd = System.Runtime.Intrinsics.X86.Avx2;

namespace CPURaytracing.VMath
{
    public class FloatBatch
    {
        public static int Count { get { return vec.Count; } }
        public static FloatBatch Zero { get { return new FloatBatch(vec.Zero); } }


        internal vec Value;
        public FloatBatch(float[] mem)
        {
            unsafe
            {
                fixed (float* m = mem)
                    Value = simd.LoadVector256(m);
            }
        }
        public FloatBatch(float v)
        {
            unsafe
            {
                Value = simd.BroadcastScalarToVector256(&v);
            }
        }
        public FloatBatch() { }
        internal FloatBatch(vec v)
        {
            Value = v;
        }

        public void Write(float[] buf, int offset)
        {
            unsafe
            {
                fixed (float* m = &buf[offset])
                    Value = simd.LoadVector256(m);
            }
        }
        public void Read(float[] buf, int offset)
        {
            unsafe
            {
                fixed (float* f_p = &buf[offset])
                    simd.Store(f_p, Value);
            }
        }

        public static FloatBatch Sqrt(FloatBatch a)
        {
            return new FloatBatch(simd.Sqrt(a.Value));
        }
        public static bool AllGreater(FloatBatch a, FloatBatch b)
        {
            var res = simd.Compare(a.Value, b.Value, System.Runtime.Intrinsics.X86.FloatComparisonMode.OrderedGreaterThanNonSignaling);
            unsafe
            {
                float[] buf = new float[Count];
                fixed (float* f_p = buf)
                    simd.Store(f_p, res);

                for (int i = 0; i < buf.Length; i++)
                    if (buf[i] != 1)
                        return false;

                return true;
            }
        }

        public static FloatBatch operator -(FloatBatch a, FloatBatch b)
        {
            return new FloatBatch(simd.Subtract(a.Value, b.Value));
        }
        public static FloatBatch operator *(FloatBatch a, FloatBatch b)
        {
            return new FloatBatch(simd.Multiply(a.Value, b.Value));
        }
        public static FloatBatch operator -(FloatBatch a)
        {
            return new FloatBatch(simd.Subtract(vec.Zero, a.Value));
        }
    }
}
